import os
import imghdr
from flask import Flask, render_template, make_response, redirect, url_for
from flask.ext.bootstrap import Bootstrap
from flask.ext.wtf import Form
from wtforms import FileField, SubmitField, SelectField, ValidationError,TextField
import subprocess 
from flask_socketio import SocketIO
app = Flask(__name__)
app.config['SECRET_KEY'] = 'top secret!'
bootstrap = Bootstrap(app)
socketio = SocketIO(app)
lst=[]

def fill_list():
    count = 0
    for file in os.listdir(os.path.join(app.static_folder, "scripts")):
        if file[-2:]=='py':
            lst.append((count,file[:-3]))
            count+=1

class PipelineForm(Form):
    select_op = SelectField('Apply Operation',coerce=int,choices=lst)
    submit = SubmitField('Submit')


class UploadForm(Form):
    image_file = FileField('Image file')
    ques=TextField('Ask any question')
    submit = SubmitField('Submit')
ques=None
image = None
pipeline = []
result=[]
@app.route('/', methods=['GET', 'POST'])
def index():
    global image,pipeline,result

    uform = UploadForm()
    pform = PipelineForm()

    if pform.validate_on_submit():
        operation = pform.select_op.data
        op = lst[operation][1]
        pipeline.append(op)

        if image != None:
            result[:]=[]
            file_location = os.path.join(app.static_folder, image)
            script_location = os.path.join(app.static_folder, "scripts/"+pipeline[-1]+".py")
            print file_location, script_location
            proc = subprocess.Popen(['python',script_location,file_location],stdout=subprocess.PIPE)
            while True:
              line = proc.stdout.readline()
              if line != '':
                 result.append(line.rstrip())
              else:
                 break
            #call(["python",script_location,file_location])
            #fi=open('static/scripts/rs.txt')

           # p = Popen([call(["python",script_location,file_location])], stdin=PIPE, stdout=PIPE, stderr=PIPE)
           #output, err = p.communicate(b"input data that is passed to subprocess' stdin")
           #rc = p.returncode
        else:
            pipeline=[]


    elif uform.validate_on_submit():
        if hasattr(uform.image_file.data,'filename'):
            pipeline = []
            for file in os.listdir(app.static_folder+"/temp"):
                os.remove(app.static_folder+"/temp/"+file)
            image = 'temp/' + uform.image_file.data.filename
            uform.image_file.data.save(os.path.join(app.static_folder, image))
  

    return render_template('index.html', uform=uform, pform=pform, image=image, pipeline = pipeline,result=result)

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response
@socketio.on('message')
def handle_message(message):   
 print(message)
if __name__ == '__main__':
    fill_list()
    socketio.run(app)
